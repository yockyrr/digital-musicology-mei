using Gee;
using GXml;

/**
  * A source of Notre Dame notation
  */
class Source : Printer {
	/**
	  * Name of the source
	  */
	public string name {
		get;
		private set;
	}
	/**
	  * Set of differences with other sources
	  */
	private TreeSet<Diff ? > ls;
	/**
	  * The elements of the source which are completely concordant with
	  * other
	  * sources
	  */
	private Elements ? els;
	/**
	  * Constructor of the source
	  *
	  * @param name Name of the source
	  */
	public Source(string name) {
		/* initialise the diffs and elements */
		ls = new TreeSet<Diff ? >();
		els = new Elements();
		this.name = name;
	}
	/**
	  * Find the differences with another source
	  *
	  * @param from Source to check against
	  * @return A diff that contains the differences with this source.
	  */
	public Diff ? find_diff(Source ? from) {
		/* iterate over diffs */
		foreach(var d in ls)
			if(d.source_equals(from))
				return d;
		/* diff with source not found, create new, empty diff with this
		  * source
		  */
		var d = new Diff(from);
		ls.add(d);
		return d;
	}
	/**
	  * Output function
	  *
	  * @param indent Indent level to print at
	  */
	public void print_diffs(int indent) {
		print_indent(indent);
		stdout.printf("Source '%s':\n", name);
		/* For each diff, print it at a higher indent level */
		foreach(var d in ls)
			d.print_diffs(indent + 1, this);
		return;
	}
	/**
	  * Add a concordant element
	  *
	  * @param el Tag name of the element
	  */
	public void add_el(string el) {
		els.set(el);
	}
	/**
	  * Get a concordant element
	  *
	  * @param el Tag name of the element
	  * @return Number of concordant elements with this tag name
	  */
	public int get_el(string el) {
		return els.get(el);
	}
}
/**
  * List of concordant elements in a source
  */
class Elements : Printer {
	/**
	  * Map of tag names to number of occurrences of that tag name.
	  */
	private TreeMap<string, int> els;
	/**
	  * Constructor
	  */
	public Elements() {
		/* initialise elements */
		els = new TreeMap<string, int>();
	}
	/**
	  * Add a concordant element.
	  *
	  * @param el Tag name of the element
	  */
	public void @set(string el) {
		/* Does the element already exist in the map? */
		if(!els.has_key(el)) {
			els.set(el, 1);
			return;
		}
		/* Increment mapping */
		var c = this.get(el);
		els.set(el, c + 1);
		return;
	}
	/**
	  * Get the number of concordant elements of this tag name.
	  *
	  * @param el Tag name of the element
	  * @return Number of concordant elements with this tag name.
	  */
	public int @get(string el) {
		return els.get(el);
	}
	/**
	  * Output function
	  *
	  * @param indent Indent level to print at
	  * @param source Source to check against
	  */
	public void print_diffs(int indent, Source ? s) {
		foreach(var d in els.entries) {
			var el = s.get_el(d.key);
			print_indent(indent + 1);
			stdout.printf("%s: %d/%d\n", d.key, d.value,
			              el + d.value);
		}
		return;
	}
}
/**
  * List of differences in a source
  */
class Diff : Printer {
	/**
	  * The source in question
	  */
	private unowned Source ? src;
	/**
	  * The elements that differ
	  */
	private Elements ? diffs;
	/**
	  * Constructor
	  *
	  * @param from The source that we're checking against
	  */
	public Diff(Source ? from) {
		src = from;
		/* initialise elements */
		diffs = new Elements();
	}
	/**
	  * Are we talking about the same source?
	  */
	public bool source_equals(Source ? b) {
		if(b == src)
			return true;
		return false;
	}
	/**
	  * Add an element to the differences.
	  *
	  * @param el Tag name of the element.
	  */
	public void add(string el) {
		diffs.set(el);
	}
	/**
	  * Output function
	  *
	  * @param indent Indent level to print at
	  * @param source Source to check against
	  */
	public void print_diffs(int indent, Source ? s) {
		print_indent(indent);
		stdout.printf("Diff with source '%s':\n", src.name);
		diffs.print_diffs(indent + 1, s);
	}
}
/**
  * Class from which all classes derive from.
  */
class Printer {
	/**
	  * The super indent-printing function.
	  *
	  * @param indent Number of indents to print.
	  */
	protected void print_indent(int indent) {
		for(int i = 0; i < indent; ++i)
			stdout.printf("  ");
		return;
	}
}
/**
  * Super class for MEI.
  */
class MEIDiff : Printer {
	/**
	  * List of sources in the file
	  */
	private TreeSet<Source ? > sources;
	/**
	  * Constructor.
	  */
	public MEIDiff() {
		/* initialise sources */
		sources = new TreeSet<Source ? >();
	}
	/**
	  * Add a source to the list.
	  *
	  * @param name Name of this source
	  */
	public Source ? add_source(string name) {
		Source ? s = new Source(name);
		sources.add(s);
		return s;
	}
	/**
	  * Find a source that goes by a name.
	  *
	  * @param name Name of the source.
	  */
	private Source ? find_source(string name) {
		/* iterate over sources until you find it. */
		foreach(var s in sources)
			if(s.name == name)
				return s;
		/* or return a new source with that name */
		return add_source(name);
	}
	/**
	  * Return all sources NOT matching this source.
	  *
	  * @param not Return everything but this source.
	  * @return All sources but the one passed as parameter.
	  */
	private TreeSet<Source ? > other_sources(Source ? not) {
		/* Create a new empty set */
		var s = new TreeSet<Source ? >();
		/* Iterate over sources and add to the set if not equal to not
		  * */
		foreach(var t in sources)
			if(t != not)
				s.add(t);
		return s;
	}
	/**
	  * Insert a difference between two sources.
	  *
	  * @param s Source in question
	  * @param from Source to compare against
	  * @param element Tag name of the element
	  */
	private void insert_diff(Source ? s, Source ? from, string element) {
		var d = s.find_diff(from);
		d.add(element);
	}
	/**
	  * Add a diff between a source and an element.
	  *
	  * @param source Name of the source
	  * @param element Tag name of the element
	  */
	public void add_diff(string source, string element) {
		var s = find_source(source);
		/* Add a diff for every other source */
		foreach(var t in other_sources(s))
			insert_diff(t, s, element);
	}
	/**
	  * Add a normal concordant element to a source.
	  *
	  * @param source Name of the source
	  * @param element Tag name of the element
	  */
	public void add_el(string source, string element) {
		var s = find_source(source);
		s.add_el(element);
	}
	/**
	  * Output function
	  *
	  * @param indent Indent level to print at
	  */
	public void print_diffs(int indent) {
		print_indent(indent);
		stdout.printf("Differences:\n");
		foreach(var s in sources)
			s.print_diffs(indent + 1);
		return;
	}
	/**
	  * Main callable function.
	  *
	  * Parse a MEI file.
	  *
	  * @param path Local path to MEI file.
	  */
	public void parse_file(string path) {
		try {
			/* Open file */
			var f = File.new_for_path(path);
			/* Parse with XML reader */
			var doc = new GDocument.from_file(f);
			/* Recurse once to find all sources */
			find_sources(doc.document_element);
			/* Recurse again to parse elements */
			parse_node(doc.document_element);
		} catch(GLib.Error e) {
			stderr.printf("Error: %s\n", e.message);
			return;
		}
	}
	/**
	  * First pass over the file.
	  *
	  * @param node Base element of the document.
	  */
	private void find_sources(DomElement ? node) {
		foreach(var child in node.children) {
			/* Find rdg-s */
			if(child.tag_name == "rdg") {
				if(!child.has_attribute("source")) {
					stderr.printf("No source prop\n");
					continue;
				}
				/* Get the source attribute */
				var prop = child.get_attribute("source");
				/* Split up into individual sources, cut the
				  * pound signs off
				  */
				var srcs = prop[1 : prop.length].split(" #");
				foreach(var s in srcs)
					find_source(s);
			}
			/* Recurse! */
			find_sources(child);
		}
		return;
	}
	/**
	  * Second pass over the file
	  *
	  * @param node Base element of the document.
	  */
	private void parse_node(DomElement ? node) {
		foreach(var child in node.children) {
			/* Find rdg-s */
			if(child.tag_name == "rdg") {
				if(!child.has_attribute("source")) {
					stderr.printf("No source prop\n");
					continue;
				}
				/* Get the source attribute */
				var prop = child.get_attribute("source");
				/* Split up into individual sources, cut the
				  * pound signs off
				  */
				var srcs = prop[1 : prop.length].split(" #");
				/* In each child, parse everything as diff */
				foreach(var s in srcs) {
					var t = find_source(s);
					parse_diff(child, t);
				}
			} else {
				/* Else just parse everything as a normal
				  * element */
				parse_all(child);
				parse_node(child);
			}
		}
	}
	/**
	  * Recursively parse readings.
	  *
	  * @param node Base element of diff.
	  * @Param s Source to diff against.
	  */
	private void parse_diff(DomElement ? node, Source ? s) {
		var name = node.tag_name;
		/* Insert this node as diff in all sources except the one we're
		  * reading from
		  */
		foreach(var t in other_sources(s))
			insert_diff(t, s, name);
		/* Recurse! */
		foreach(var child in node.children)
			parse_diff(child, s);
		return;
	}
	/**
	  * Parse element's children as normal elements.
	  *
	  * @param node Base element for the children.
	  */
	private void parse_all(DomElement ? node) {
		var name = node.tag_name;
		foreach(var s in sources)
			s.add_el(name);
	}
}
/**
  * Main function
  *
  * @param args First arg must be a path to a valid MEI file
  */
int main(string[] args) {
	if(args.length < 2) {
		stderr.printf("MEI file required.\n");
		return 1;
	}
	/* Create instance */
	var diff = new MEIDiff();
	/* Parse file */
	diff.parse_file(args[1]);
	/* Print differences */
	diff.print_diffs(0);
	return 0;
}
