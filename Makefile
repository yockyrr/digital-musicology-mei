all: benedicamus-mensural.mei benedicamus-mensural.mei.gz mei-NotreDame.xml valacode
benedicamus-mensural.mei: builddir benedicamus-mensural.pxsl
	pxslcc benedicamus-mensural.pxsl > build/benedicamus-mensural.mei
benedicamus-mensural.mei.gz: builddir benedicamus-mensural.mei
	gzip -fk9 build/benedicamus-mensural.mei
mei-NotreDame.xml: builddir mei-NotreDame.pxsl
	pxslcc mei-NotreDame.pxsl > build/mei-NotreDame.xml
valacode: builddir diff.vala
	valac --pkg gee-0.8 --pkg gxml-0.18 diff.vala
	mv diff build
builddir:
	mkdir -p build
clean:
	rm -rfv build
