# Manually encoding Notre Dame Polyphony in MEI

The music of the Notre Dame school of polyphony still confounds scholars to
this day: a thirteenth–century repertory purportedly composed by two men,
Leoninus[^1] and Perotinus,[^2] though now thought to be a collaborative and
anonymous repertory created by innumerable performers over a period of decades.
Aspects of rhythm, pitch, *ficta* and performance practice are still largely
unknown and may never be teased out from the scant documentary evidence which
references this repertory. No source of this repertory is complete and could
truly be termed the *Magnus Liber Organi* that Anonymous IV makes reference
to,[^3] and three sources transmit three different, incomplete repertories.

*I-Fl MS Pluteus 29.1* (known as ‘F’) is the largest of the three sources and
transmits *organum*, *clausulae*, *conductus* and motet, both Latin and
vernacular.[^4] *D-W Cod. Guelf 628 Helmst.* (known as ‘W1’) transmits
*organum*, *clausulae* and Latin motet,[^5] and *D-W Cod. Guelf 1099 Helmst.*
(known as ‘W2’) transmits *organum* and both Latin and vernacular motet.[^6]
There is a reasonable amount of overlap in the repertories transmitted by all
three sources, such that there are very few *unicae* within the repertory. Much
of the *organum* central to the propers of the church year are transmitted in
all three manuscripts, but many differences in pitch, ligation, *divisiones*
and *clausulae* selected make the process of edition difficult.

One other genre that is particularly well transmitted is the *Benedicamus
Domino*, examples of which exist in all three sources. One case can be found in
F (fol. 86v), W1 (fol. 94v–95) and W2 (fol. 90v–91). Is it possible to encode
this two–part polyphony in MEI using the critical apparatus to explicate the
differences between the sources?

## 1. A Transitional Notation

Lying somewhere between neumatic chant notation and the *Ars Nova* of the
fourteenth century, the modal notation of the Notre Dame school is a dynamic
notation, very much in flux when it was written at the end of the thirteenth
century. Staff notation and clefs are largely agreed from gregorian chant, but
rhythm and the synchronisation of voices is very much an interpretative art. In
three– and four–part music, as well as *conductus* that were perhaps derived
from vernacular sources such as motet, rhythm can easily be inferred by a
regular grouping of ligatures, the Perotinian modal rhythm that Anonymous IV
bases his treatise upon. However, the rhythm of the earlier layer of Leoninian
two–part music is largely unknown, due to irregular ligatures and no clear
rules for rhythm.

In this encoding, I therefore wish to leave out any rhythm, as it would be
entirely editorial and we cannot be certain whether the rhythm is present in
the source, or is our own later addition.

## 2. MEI Contexts and Broken Tools

It is clear from this type of notation that the traditional CMN (Common Music
Notation) contexts of MEI will not suffice for this style as CMN is based on
the root iterative element of `<measure>` with a stable period. This music is
neither measure based nor will it be encoded as rhythmic (although it is not
true to say that it is not rhythmic, just that the rhythm is either unknown or
extemporised).

The MEI ‘neumes’ module appeared to be a good starting point for my encoding,
as it is intended to generate output that looks very similar to Notre Dame
notation. However, MEI support for the neumes module is purely monophonic and
it cannot be expanded to synchronise polyphony without substantial changes.
This is because the base iterative element of MEI neumatic notation is the
`<syllable>` which can only contain `<neume>` and others. This means that music
in `<section>` can only be aligned at the syllable level, and the syllables
must be attached to the entire polyphonic section.

Upon suggestion from the MEI mailing list, another option was used, the
‘mensural’ module which has better support for polyphony, although it does not
support modal rhythm. The mensural module cannot synchronise the music
directly, but does not have a reliance on `<measure>` and can make use of the
attribute `@synch`, which allows elements to be tagged as occurring
simultaneously with another element. This way, the music can be encoded
voice–at–once, and then the tenor notes can be tagged with the IDs of the upper
voice with `@synch` to demonstrate their alignment. The shape of each note can
be shown using the attribute `@lig`.

Two other elements that MEI does not natively support are *divisiones* and
*plicae*. *Divisiones* are vertical lines in the score that can function as
rests, syllable demarcation lines, or merely lines that show the alignment of
voices. It is semantically incorrect to encode them as rests or barlines, even
when it is clear that they are intended as such, therefore a new element is
required in MEI, like `<line>` but with more specificity to show its intention
as a *divisio*. The same is true for *plicae*, where the note’s pitch is not
shown, but implied by the direction of a stem at the end of a ligature.
Fortunately, MEI is customisable using ODD, so it should be possible to
customise the MEI mensural schema to generate a custom schema with a new
element called `<divisio>` and a note–containing element called `<plica>`.

Unfortunately, the MEI customisation service has been non-functioning for a few
months without a fix. MEI’s upstream, TEI, have a tool called *Roma* that can
achieve the same results, but it appears that the MEI base customisations,
although accepted by *Roma*, fail when pulling in an SVG schema. This same
error occurs on my local machine with *Saxon*. I filed a bug report with MEI on
the 26th March, and the error was traced three days later to a transport issue
in the MEI schema.[^8] This is treated as a critical bug and will result in a
bugfix to the schemata.

Once the issue had been fixed locally, I then constructed an [ODD
file](mei-NotreDame.pxsl) which added the `<divisio>` and `<plica>` elements.
The `<divisio>` element functions exactly like a barline, but `<plica>` is
modelled from `<ligature>` and can contain anything that `<ligature>` can.
`<plica>` also pulls in the attributes of `att.stems` so that the direction of
the plica can be marked as well as an `<nc>` element indicating an editorial
pitch.

## 3. Hand–encoding MEI

Obtaining the base texts of this *Benedicamus Domino* using tools such as
SibMEI or a MusicXML conversion is not useful, as these tools are all based
upon CMN and will require significant processing to reformat, restructure and
remove the errant information, such as duration. I instead elected to encode
the file by hand, directly in MEI. XML is an extremely verbose format,
requiring tags to be opened and closed extremely frequently. Furthermore, MEI
is not a particularly terse XML, with long tag and attribute names. One
shortcut that I used to type XML, along with various text processing tools, was
PXSL, or *Parsimonious XML Shorthand Language*.[^7] PXSL is a shorthand for XML
that uses whitespace and indent to auto open and close tags, as well as quoting
attributes correctly.

```xml
<ligature xml:id="testme">
  <note pname="c" oct="3"/>
</ligature>
```

in XML is simply written as:

```
ligature -xml:id=testme
  note -pname=c -oct=3
```

in PXSL. The PXSL is then compiled to XML using *pxslcc*.

One tool I did use in order to generate zones for facsimile was TEI’s *Image
Markup Tool* (IMT).[^9] IMT is an easy to use facsimile zone marker which
generates near–compliant MEI facsimile zones that only need a little processing
using VIM to paste directly in PXSL MEI:

```viml
" After deleting all the lines in the file except the long line that contains the <zone> tags.
" put all tags on a new line
%s/</\r</g
" delete from the final <zone> tag to the bottom
execute "normal! G?zone\<cr>VGd"
" delete closing tag lines
g/<\/zone>/d
" remove front tag
%s/<zone/zone/
" remove quotes
%s/"//g
" delete attribute 'rendition'
%s/rendition=Default //
" delete from 'rend' to the end
%s/rend.\+$//
" add hyphens to each attribute
%s/ / -/g
" remove extra hyphens added at the end
%s/ -$//
```

## 4. Showing the Differences

Given that this MEI is non-compliant and its notation type will not be
supported by any currently–available tools, I decided to use generic XML
processing to iterate through the XML and generate statistics on the
differences between the sources. I [wrote a program](diff.vala) in *Vala* that
generates statistics on how many sources have changed elements within `<rdg>`
elements. It first finds a list of all the sources described in the file, and
then for each `<rdg>`, counts the number of elements of each type within that
have changed from the other readings. Running `./diff benedicamus-mensural.mei`
outputs:

```
Differences:
  Source 'w1':
    Diff with source 'w2':
        app: 1/89
        clef: 2/2
        divisio: 47/49
        ligature: 55/175
        nc: 7/7
        note: 126/387
        plica: 7/7
        rdg: 90/90
        staffDef: 1/2
        syl: 2/8
    Diff with source 'f':
        app: 1/89
        divisio: 44/46
        ligature: 56/177
        nc: 7/7
        note: 129/390
        plica: 7/7
        rdg: 90/90
        staffDef: 1/2
        syl: 1/7
  Source 'w2':
    Diff with source 'w1':
        app: 1/89
        clef: 2/2
        divisio: 40/42
        ligature: 45/165
        nc: 6/6
        note: 117/378
        plica: 6/6
        rdg: 90/90
        staffDef: 1/2
        syl: 2/8
    Diff with source 'f':
        app: 1/89
        divisio: 44/46
        ligature: 57/177
        nc: 7/7
        note: 129/390
        plica: 6/6
        rdg: 90/90
        staffDef: 1/2
        syl: 2/8
  Source 'f':
    Diff with source 'w1':
        app: 1/89
        clef: 2/2
        divisio: 40/42
        ligature: 45/165
        nc: 6/6
        note: 117/378
        plica: 6/6
        rdg: 90/90
        staffDef: 1/2
        syl: 2/8
    Diff with source 'w2':
        app: 1/89
        clef: 2/2
        divisio: 47/49
        ligature: 55/175
        nc: 7/7
        note: 126/387
        plica: 7/7
        rdg: 90/90
        staffDef: 1/2
        syl: 2/8
```

This shows that, for example, source W1 when compared to W2 has 126 notes out
of 387 that are different, making W2 roughly 67% similar in terms of notes to
W1. However, when it comes to *divisiones*, only two in W2 are identical to W1,
making *divisiones* in W2 only 5% similar to W1. The full percentages are
below:

| Source | Target | Element  | Similarity |
| ------ | ------ | -------- | ---------- |
| W1     | W2     | Clef     | 0%         |
|        |        | Divisio  | 4%         |
|        |        | Ligature | 69%        |
|        |        | Note/Nc  | 67%        |
|        |        | Plica    | 0%         |
|        |        | Syllable | 75%        |
|        | F      | Divisio  | 4%         |
|        |        | Ligature | 68%        |
|        |        | Note/Nc  | 66%        |
|        |        | Plica    | 0%         |
|        |        | Syllable | 86%        |
| W2     | W1     | Clef     | 0%         |
|        |        | Divisio  | 5%         |
|        |        | Ligature | 73%        |
|        |        | Note/Nc  | 67%        |
|        |        | Plica    | 0%         |
|        |        | Syllable | 75%        |
|        | F      | Divisio  | 4%         |
|        |        | Ligature | 68%        |
|        |        | Note/Nc  | 66%        |
|        |        | Plica    | 0%         |
|        |        | Syllable | 75%        |
| F      | W1     | Clef     | 0%         |
|        |        | Divisio  | 5%         |
|        |        | Ligature | 73%        |
|        |        | Note/Nc  | 68%        |
|        |        | Plica    | 0%         |
|        |        | Syllable | 75%        |
|        | W2     | Clef     | 0%         |
|        |        | Divisio  | 4%         |
|        |        | Ligature | 69%        |
|        |        | Note/Nc  | 66%        |
|        |        | Plica    | 0%         |
|        |        | Syllable | 75%        |

## 5. Implications, Limitations

We can conclude from the above data that pitch content is largely similar in
the concordant settings of *Benedicamus Domino*. However, *divisiones* are
rarely transmitted the same, because the length or placement of the *divisione*
appears inconsequential in the source, and each source has its own
process of *divisio* placement in keeping with the hand and style of the
source.

This kind of analysis is very limited however, as it only counts the number of
differences, not the size of each difference. A *divisione* that is shifted
vertically by one place (a seeming non-issue) counts just as much in this
analysis as a ‘difference’ as a pitch change or other more significant
difference.

Furthermore, the granuality of the readings remains in question. I have encoded
each source’s reading at the level of the ligature, such that a change of one
note in a ligature causes the entire ligature to be re-encoded. This amounts to
a greater apparent change than what actually occurred in the source. A better
methodology would be to calculate not the number of changes but the edit
distance between the changes. However, this has not been done here as
implementing edit distances can be a complex matter, especially in tree
structures such as XML where there are few satisfactory algorithms to generate
edit scores.

[^1]: Roesner, E. (2001, January 01). Leoninus *Grove Music Online*. Retrieved
27 March 2019 from
http://www.oxfordmusiconline.com.ezproxy.lib.gla.ac.uk/grovemusic/view/10.1093/gmo/9781561592630.001.0001/omo-9781561592630-e-0000040466
[^2]: Roesner. E. (2001, January 01). Perotinus *Grove Music Online*. Retrieved
27 March 2019 from
http://www.oxfordmusiconline.com.ezproxy.lib.gla.ac.uk/grovemusic/view/10.1093/gmo/9781561592630.001.0001/omo-9781561592630-e-0000040465
[^3]: Dittmer. L. A. (1959). *Anonymous IV*. Brooklyn, NY: Institute of
Mediaeval Music. Retrieved 27 March 2019 from
https://archive.org/details/anonymousivconce00ditt
[^4]: I-Fl MS Pluteus 29.1 (Medici Antiphoner, ('F')), *Digital Image Archive
of Medieval Music*. Retrieved 27 March 2019 from
https://www.diamm.ac.uk/sources/924
[^5]: D-W Cod. Guelf. 628 Helmst. (Magnus Liber Organi; W1), *Digital Image
Archive of Medieval Music*. Retrieved 27 March 2019 from
https://www.diamm.ac.uk/sources/870
[^6]: D-W Cod. Guelf 1099 Helmst. (W2), *Digital Image Archive of Medieval
Music*. Retrieved 27 March 2019 from https://www.diamm.ac.uk/sources/854
[^7]: Moertel, T. (n.d.) Parsimonious XML Shorthand Language. Retrieved 27
March 2019 from https://github.com/tmoertel/pxsl-tools
[^8]: Stutter, J. (2019, March 26). Build fails on SVG. Retrieved 29 March 2019
from https://github.com/music-encoding/music-encoding/issues/581
[^9]: Holmes, M. (n.d.) The UVic Image Markup Tool Project. Retrieved 27 March 2019 from http://tapor.uvic.ca/~mholmes/image_markup/index.php
